<?php

use Illuminate\Database\Seeder;
use App\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $m = new Module;
        $m->name = "Programming";
        $m->save();

        $m = new Module;
        $m->name = "Maths";
        $m->save();
    }
}

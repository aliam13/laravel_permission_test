<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p = new Permission;
        $p->name = "create-tasks";
        $p->save();

        $p = new Permission;
        $p->name = "view-tasks";
        $p->save();

        $p = new Permission;
        $p->name = "edit-tasks";
        $p->save();

        $p = new Permission;
        $p->name = "delete-tasks";
        $p->save();
    }
}

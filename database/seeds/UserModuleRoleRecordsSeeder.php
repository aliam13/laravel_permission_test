<?php

use Illuminate\Database\Seeder;
use App\UserModuleRoleRecord;
use App\User;

class UserModuleRoleRecordsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alex = User::find(1);
        $bob = User::find(2);

        // Alex is a Viewer on Programming
        $alex->userModuleRoleRecords()->create(['role_id' => 1 , 'module_id' => 1]);
        // Alex is a Coordinator on Maths
        $alex->userModuleRoleRecords()->create(['role_id' => 2 , 'module_id' => 2]);
        // Bob is a Viewer on Maths
        $bob->userModuleRoleRecords()->create(['role_id' => 1 , 'module_id' => 2]);
    }
}

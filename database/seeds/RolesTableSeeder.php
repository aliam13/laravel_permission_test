<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $r = new Role;
        $r->name = "viewer";
        $r->save();

        $r = new Role;
        $r->name = "coordinator";
        $r->save();
    }
}

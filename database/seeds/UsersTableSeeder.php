<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $u = new User;
        $u->name = "Alex";
        $u->email = "alex@example.com";
        $u->password = Hash::make('secret');
        $u->save();

        $u = new User;
        $u->name = "Bob";
        $u->email = "bob@example.com";
        $u->password = Hash::make('secret');
        $u->save();
    }
}

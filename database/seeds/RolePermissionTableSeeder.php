<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\Permission;

class RolePermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$createTasksPermission = Permission::findByName("create-tasks");
    	$viewTasksPermission = Permission::findByName("view-tasks");
    	$editTasksPermission = Permission::findByName("edit-tasks");
    	$deleteTasksPermission = Permission::findByName("delete-tasks");

        $r = Role::findByName('viewer');
        $r->permissions()->saveMany([$viewTasksPermission]);

        $r = Role::findByName('coordinator');
        $r->permissions()->saveMany([$createTasksPermission, $viewTasksPermission, $editTasksPermission, $deleteTasksPermission]);
    }
}

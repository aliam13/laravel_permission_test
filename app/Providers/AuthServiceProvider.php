<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create-tasks', function ($user, \App\Module $module) {
            return $module->checkPermissionForUser('create-tasks', $user);
        });

        Gate::define('view-tasks', function ($user, \App\Module $module) {
            return $module->checkPermissionForUser('view-tasks', $user);
        });
    }
}

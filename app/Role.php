<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public static function findByName($name)
    {
        $x = static::where('name', $name)->first();

        if (!$x) {
            throw new \Exception("Permission '$name' not found.");
        }

        return $x;
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }

    public function userModuleRoleRecords()
    {
        return $this->hasMany('App\UserModuleRoleRecord');
    }

}

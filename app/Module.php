<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function userModuleRoleRecords()
    {
        return $this->hasMany('App\UserModuleRoleRecord');
    }

    public function checkPermissionForUser(String $permissionName, User $user) {
    	$permissions = DB::table('user_module_role_records')
                        ->select('permissions.name')
                        ->distinct()
                        ->where('user_module_role_records.module_id', '=', $this->id)
                        ->where('user_module_role_records.user_id', '=', $user->id)
                        ->join('permission_role', 'user_module_role_records.role_id', '=', 'permission_role.role_id')
                        ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
                        ->where('permissions.name', '=', $permissionName)
                        ->get();

        return $permissions->isNotEmpty();
          // A more Eloquent way to do this
          // $permission = Permission::findByName($permissionName);
          // $roleRecords = $this->userModuleRoleRecords()->where('user_id', $user->id)->get();

          // foreach ($roleRecords as $roleRecord) {
          //    if ($roleRecord->role->permissions->contains($permission)) {
          //        return true;
          //    }
          //}

          // return false;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public static function findByName($name)
    {
        $x = static::where('name', $name)->first();
        if (!$x) {
    		throw new \Exception("Permission '$name' not found.");
    	}

        return $x;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
}

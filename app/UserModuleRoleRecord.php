<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserModuleRoleRecord extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function module()
    {
        return $this->belongsTo('App\Module');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('modules/{module}/tasks/create', function (App\Module $module) {
   	return "Displaying task creation page for $module->name module";
})->middleware('can:create-tasks,module');

Route::get('modules/{module}/tasks/view', function (App\Module $module) {
   	return "Displaying task view page for $module->name module";
})->middleware('can:view-tasks,module');